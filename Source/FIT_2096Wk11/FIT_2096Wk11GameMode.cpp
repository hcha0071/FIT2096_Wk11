// Copyright Epic Games, Inc. All Rights Reserved.

#include "FIT_2096Wk11GameMode.h"
#include "FIT_2096Wk11HUD.h"
#include "FIT_2096Wk11Character.h"
#include "UObject/ConstructorHelpers.h"

AFIT_2096Wk11GameMode::AFIT_2096Wk11GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFIT_2096Wk11HUD::StaticClass();
}
