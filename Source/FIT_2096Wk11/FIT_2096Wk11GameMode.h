// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FIT_2096Wk11GameMode.generated.h"

UCLASS(minimalapi)
class AFIT_2096Wk11GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFIT_2096Wk11GameMode();
};



