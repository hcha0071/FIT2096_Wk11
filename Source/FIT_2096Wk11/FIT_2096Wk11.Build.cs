// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class FIT_2096Wk11 : ModuleRules
{
	public FIT_2096Wk11(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
